# Generated by Django 4.2 on 2023-04-19 20:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0005_alter_receipt_vendor"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="account",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="receipts",
                to="receipts.account",
            ),
        ),
    ]
